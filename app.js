//Selectors
const elPay = document.getElementById('pay');
const elBalance = document.getElementById('balance');
const elDropdown = document.getElementById('dropdown');
const elView = document.getElementById('view');
const elFeatures = document.getElementById('features');

// Variables for the transactions
const rate = 100;
let pay = 0;
let balance = 0;
let eligbleForLoan = true;
const laptopList = [
    {id: 1, model: 'Orange 3000', price: 800, description: "A small and reliable computer.", features: ['Nice screen', 'Longlasting battery'], imgsrc: 'https://www.publicdomainpictures.net/pictures/10000/nahled/1-1216221458n7mg.jpg'},
    {id: 2, model: 'Pai X90C', price: 1500, description: "Small, fast and powerful. The deadly trio.", features: ['RGB lights', 'Longlasting battery'], imgsrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSQ2r55INF41zDSTcpMq4K0cFGabE-KSG-xhQ&usqp=CAU'},
    {id: 3, model: 'JuiceMaster X', price: 4000, description: "The most powerful piece of hardware for personal use money can buy.", features: ['Next-gen GPU', 'Latest CPU'], imgsrc: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Apple-desk-laptop-macbook_%2824299816666%29.jpg/1024px-Apple-desk-laptop-macbook_%2824299816666%29.jpg'},
    {id: 4, model: 'Yung AC40', price: 2200, description: "Everything works. It just works.", features: ['Webcam', '16GB RAM', '1TB SSD'], imgsrc: 'https://www.publicdomainpictures.net/pictures/170000/nahled/laptop-1462985546Xx3.jpg'}
];


// Set default values on load
window.onload = () => {
    elPay.innerText = `Pay ${pay} kr.`;;
    elBalance.innerText = `Balance ${balance} kr.`;;
    laptopList.forEach(laptop => {
        let option = document.createElement('option');
        option.text = laptop.model;
        option.value = laptop.id;
        elDropdown.add(option);
    });
    showChosenView();
}

// When Work-button is pressed
const earnPayment = () => {
    pay+=rate;
    elPay.innerText = `Pay ${pay} kr.`;
}

// When Bank-button is pressed
const transferToBank = () => {
    if(pay > 0) {
        balance+=pay;
        pay = 0;
        elPay.innerText = `Pay ${pay} kr.`;
        elBalance.innerText = 'Balance ' + balance + ' kr.';
    } else {
        alert('No payment available to transfer to bank account.');
    }
}

// When Loan-button is pressed
const getLoan = () => {
    if(eligbleForLoan && balance > 0) {
        let amount = parseInt(prompt('Enter amount to loan.'));

        if((amount !== null) && (amount > 0) && (typeof amount === 'number')) {
            if(amount <= balance*2) {
                balance+=amount;
                eligbleForLoan = false; // Needs to be set to true after purchase
                elBalance.innerText = `Balance ${balance} kr.`;
            } else {
                alert("Amount requested can't be more than twice your current bank balance.");
            }
        } else {
            alert('Not a valid input.');
        }
    } else {
        alert("You're not eligble for a loan. Check if bank balance is more than 0. You can't get a new loan before buying a computer.");
    }
}

// Show the chosen computer in the view div
const showChosenView = () => {
    elView.innerHTML = '';
    elFeatures.innerHTML = '';

    let selectedId = elDropdown.options[elDropdown.selectedIndex].value;
    let laptop = laptopList.find(laptop => laptop.id == selectedId);

    const elLeftContainer = document.createElement('div');
    const elMiddleContainer = document.createElement('div');
    const elRightContainer = document.createElement('div');
    const elImg = document.createElement('img');
    const elModel = document.createElement('h3');
    const elDesc = document.createElement('p');
    const elPrice = document.createElement('h3');
    const elBtn = document.createElement('button');


    // Add IDs to the elements
    elLeftContainer.id = 'leftContainer';
    elMiddleContainer.id = 'middleContainer';
    elRightContainer.id = 'rightContainer'; 
    elImg.id = 'chosenImg';
    elModel.id = 'chosenName';
    elDesc.id = 'chosenDesc';
    elPrice.id = 'chosenPrice';
    elBtn.id = 'chosenBtn';

    
    // Set text and attributes to the elements
    elImg.src = laptop.imgsrc;
    //elImg.setAttribute('src', laptop.imgsrc);
    elModel.innerText = laptop.model;
    elPrice.innerText = `${laptop.price} kr.`;
    elDesc.innerText = laptop.description;
    elBtn.onclick = buyComputer;
    elBtn.type = 'button';
    elBtn.textContent = 'Buy';
    
    laptop.features.forEach(feature => {
        let elFeature = document.createElement('p');
        elFeature.innerText = feature;
        elFeatures.appendChild(elFeature);
    });

    elLeftContainer.appendChild(elImg);
    elMiddleContainer.appendChild(elModel);
    elMiddleContainer.appendChild(elDesc);
    elRightContainer.appendChild(elPrice);
    elRightContainer.appendChild(elBtn);
    elView.appendChild(elLeftContainer);
    elView.appendChild(elMiddleContainer);
    elView.appendChild(elRightContainer);
}

// When Buy-button is pressed
const buyComputer = () => {
    let selectedId = elDropdown.options[elDropdown.selectedIndex].value;
    let {model, price} = laptopList.find(laptop => laptop.id == selectedId);
    
    if(balance < price) {
        alert('Not enough funds to purchase.');
        return;
    }
    
    balance -= price;
    alert(`${model} has been bought.`);
    elBalance.innerText = `Balance ${balance} kr.`;
    if(eligbleForLoan === false) eligbleForLoan = true;
}